import javax.swing.*;
import java.util.ArrayList;

/**
 * Created by AA_PROF-ESIG on 10.05.2016.
 */
public class Programme {

    public static void main(String[] args) {

        ArrayList<String> liste = new ArrayList<>();
        liste.add("Bob");
        liste.add("alice");
        liste.add("Janine");

        String valeurAChercher = JOptionPane.showInputDialog("Quelle valeur cherchez-vous?");
        MoteurDeRecherche recherche = new MoteurDeRecherche();
        int position = recherche.trouverPosition(liste, valeurAChercher);

        JOptionPane.showMessageDialog(null, "Votre valeur est en position: " + position, "Résultat", JOptionPane.INFORMATION_MESSAGE);
    }
}
